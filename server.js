import express from 'express'
import bodyParser from 'body-parser'
import pubsub from './pubsub'
import { execute, subscribe } from 'graphql'
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express'
import { createServer } from 'http'
import { SubscriptionServer } from 'subscriptions-transport-ws'

import schema from './schema'

const port = process.env.PORT || 3000
const app = express()

// Set endpoints we can hit
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }))
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql', subscriptionsEndpoint: `ws://localhost${port}/subscriptions` }))

const ws = createServer(app)

// Init server via `http` so that we can access it for websocket binding
ws.listen(port, () => {
  console.log(`GraphiQL is running on ::${port}`)
  new SubscriptionServer({ execute, subscribe, schema }, { server: ws, path: '/subscriptions' })
})

// Send Mock Data over pubsub
setInterval(() => {
  pubsub.publish('getNewDate', { getNewDate: Date.now() })
}, 3000)