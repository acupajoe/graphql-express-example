import { makeExecutableSchema } from 'graphql-tools'
import resolvers from './resolvers'

const typeDefs = `
type Query {
  # Get a set of mass readings given a date
  readings(month: Int, day: Int, year: Int): Readings
}

type Subscription {
  # Returns a new date every 3 seconds
  getNewDate: String
}

# A single day's readings
type Readings {
  date: String
  # Title of that day's feast
  title: String
  # It's possible there will be multiple citations for the day (i.e. if it's a feast)
  citations: [Citation]
}

type Citation {
  type: String
  verses: [String]
}
`
const schema = makeExecutableSchema({ typeDefs, resolvers })

export default schema