import { PubSub } from 'graphql-subscriptions'

// Creates a uniform object to send out updates with
export default new PubSub()