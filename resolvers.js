import axios from 'axios'
import pubsub from './pubsub'

// How should we resolve queries?
const resolvers = {
  Query: {
    async readings(root, args) {
      const apiURL = `http://www.ewtn.com/se/readings/readingsservice.svc/day`
      const response = await axios.get(`${apiURL}/${args.year}-${args.month}-${args.day}/en`)
      return response.data
    }
  },
  Readings: {
    // Uses return data from Query.readings
    date: (reading) => reading.Date,
    title: (reading) => reading.Title,
    citations: (reading) => reading.ReadingGroups
  },
  Citation: {
    // Uses return data from Readings.citations
    type: (citation) => citation.Name,
    verses: (citation) => {
      const verses = []
      citation.Readings.forEach(reading => {
        reading.Citations.forEach((citation) => {
          verses.push(citation.Reference)
        })
      })
      return verses
    }
  },
  Subscription: {
    getNewDate: {
      subscribe: () => pubsub.asyncIterator('getNewDate')
    }
  }
}

export default resolvers